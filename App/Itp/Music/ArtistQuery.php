<?php

namespace Itp\Music;

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

/**
 *
 */
class ArtistQuery extends \Itp\Base\Database
{

    function __construct()
    {
        # code...
        parent::__construct();
    }

    function getAll(){
        $sql = "SELECT * FROM artists ORDER BY artist_name ASC";
        $statement = static::$pdo->prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(\PDO::FETCH_OBJ);
        return $results;
    }
}


?>
