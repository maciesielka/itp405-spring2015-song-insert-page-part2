<?php

namespace Itp\Music;

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

/**
 *
 */
class Song extends \Itp\Base\Database
{
    private $id;
    private $title;
    private $artist_id;
    private $genre_id;
    private $price;

    function __construct()
    {
        parent::__construct();
    }

    function setTitle($title){
        $this->title = $title;
    }

    function setArtistId($artist_id){
        $this->artist_id = $artist_id;
    }

    function setGenreId($genre_id){
        $this->genre_id = $genre_id;
    }

    function setPrice($price){
        $this->price = $price;
    }

    function save(){
        //create the insert
        $sql = "INSERT INTO music.songs (title, artist_id, genre_id, price)
                VALUES ('$this->title', $this->artist_id, $this->genre_id, $this->price);";
        $statement = static::$pdo->prepare($sql);
        $statement = $statement->execute();
        $this->id = static::$pdo->lastInsertId();
    }

    function getTitle(){
        return $this->title;
    }

    function getId(){
        return $this->id;
    }
}


?>
