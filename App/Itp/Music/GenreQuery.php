<?php

namespace Itp\Music;

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

/**
 *
 */
class GenreQuery extends \Itp\Base\Database
{

    function __construct()
    {
        # code...
        parent::__construct();
    }

    function getAll(){
        $sql = "SELECT * FROM genres ORDER BY genre ASC";
        $statement = static::$pdo->prepare($sql);
        $statement->execute();
        $results = $statement->fetchAll(\PDO::FETCH_OBJ);
        return $results;
    }
}


?>
